---
layout: post
title: SotM Africa Sponsorship
permalink: /sponsorship/
isStaticPost: true
image: 2017-ad.jpg
---

__[Version française](/fr/sponsorship/)__

> Leaving no one behind: Emerging from a global pandemic

__Download Prospectus__:

* [English](/sotmafrica2021_en.pdf)  
* [Français](/sotmafrica2021_fr.pdf)

State of the Map Africa is a bi-annual conference of the OpenStreetMap communities across Africa. The conference brings together a diverse group of OSM contributors including mappers, users, developers, and generally GIS professionals to learn,share about OSM. The conference has been a good platform for sharing knowledge, fostering relationships, and encouraging new collaborations.

The success of the conference is entirely dependent on the financial support  of organizations like yours by enabling us to bring and host 40+ presenters and approximately 200 attendees from the OpenStreetMap Africa community representing over 30 African countries.
 
Your support will help us to increase diversity within the OSM community and improve OSM contribution and use within the continent. As our sponsors, you will:

- Connect with the OSMAfrica community and build collaborations with talented individuals from the continent.
- Build brand awareness and increase visibility for your products and services within the OSM community.
- Learn and share about the current developments in OSM both regionally and internationally.
- Support our commitment to increasing the use of open data especially OSM to solve real world problems that we face on a day to day basis.

Interested in sponsoring SotM Africa 2021? Have a look at our sponsorship packages and be sure to contact us. We will be happy to answer any questions and we will work with you to find the perfect sponsorship package.

Don't see what you are looking for? Get in touch via __sponsor__ [at] __stateofthemap__ [dot] __africa__

##### SotM Africa 2019 Sponsors
![SotM Africa 2019 Sponsors](/img/posts/sotmafrica_2019_all_sponsors.png)