---
layout: post
title: l'appel à participation 
permalink: /fr/calls/
isStaticPost: true
image: 2017-ad.jpg
---
> [English Version](/calls/)

En novembre 2021, la communauté OSM Afrique se réunira pour une autre conférence passionnante : State of the Map Africa 2021. Cette conférence a pour but de rassembler les passionnés d'OpenStreetMap d'Afrique et d'ailleurs. La conférence sera l'occasion pour les cartographes de partager et d'échanger des idées, des connaissances et des expériences, d'élargir leur réseau et de dégager des idées pour développer OSM sur le continent. Nous invitons les membres de la communauté OpenStreetMap à partager avec nous leurs projets et idées en matière de cartographie, ainsi que leurs expériences OSM, tant au niveau individuel que communautaire. Qu'il s'agisse de cartographes débutants, d'utilisateurs de données OSM ou de leaders communautaires, nous aimerions avoir de vos nouvelles !

Outre les propositions sur la contribution, l'utilisation et le développement d'OSM, le thème de cette année est "Ne laisser personne pour compte" : Sortir d'une pandémie mondiale, nous sommes désireux d'en savoir plus sur :
* Les projets de cartographie axés sur les communautés marginalisées et les minorités.
* Les projets de cartographie qui ont soutenu la lutte contre le COVID-19. 
* Comment votre communauté OSM s'est adaptée à la pandémie.
* Votre expérience individuelle de la cartographie pendant la pandémie.
* Les collaborations en matière de cartographie entre différentes entités, communautés et pays.

#### Navigation
* <a href="#tracks">Sujets<a/>
* <a href="#subtypes">Types de soumission<a/>
* <a href="#subdetails">Détails de la Soumission<a/>
* <a href="#coc">Code de conduite<a/>
* <a href="#review">Règles d'examen<a/>
* <a href="#timelines">Calendrier<a/>
* <a href="#submit">Soumission<a/>

<h4 id="tracks">Sujets</h4>
Nous recherchons des propositions dans ces catégories :
* **Cartographie et visualisation de données** Cette piste concerne tout ce qui a trait à la cartographie et à la visualisation de données. Nous accueillons toutes les soumissions qui démontrent l'utilisation des données OpenStreetMap et des plateformes, outils et logiciels comme QGIS, GrassGIS, Blender, etc., pour produire et partager des cartes et des visualisations convaincantes.
* **Cartographie** Il existe plusieurs façons de contribuer à OSM et d'éditer la carte ! Dans cette piste, nous aimerions savoir comment vous utilisez les outils existants et nouveaux, les applications mobiles et les plateformes pour la collecte, l'édition et la validation des données. Cela inclut également les expériences de test, les avantages et les inconvénients de chacun, etc.
* **Analyse des données** Cette piste concerne les recherches, les projets et les travaux scientifiques réalisés pour évaluer la qualité des données OpenStreetMap, les modèles de contribution et l'utilisation.
* **Communauté** Cette piste est axée sur les présentations de communautés et de réseaux ouverts dans le domaine de la cartographie ouverte et de l'espace géospatial en général. Qu'il s'agisse de communautés OSM locales et régionales, de chapitres YouthMappers, de chapitres OSGeo ou d'autres réseaux de groupes, nous aimerions entendre parler de vos activités communautaires, de vos plans de durabilité et de vos initiatives visant à améliorer la croissance, la diversité et l'inclusion de la communauté.
* **Volet innovation** Avez-vous construit des solutions web en utilisant OSM ? Nous aimerions connaître votre processus de conception et de développement d'applications web, de portails et de cartes narratives à l'aide de l'API OpenStreetMap, de différentes bibliothèques et de plateformes tierces.
* **Informatique scientifique** Cette piste est dédiée aux personnes et/ou organisations qui souhaitent partager leurs cas d'utilisation de la cartographie avec l'intelligence artificielle, l'apprentissage automatique, la technologie de la réalité (AR/VR/MR), etc.

<h4 id="subtypes">Types de soumission</h4>
* **Exposés** Cette session est principalement dédiée au partage de votre travail avec la communauté OSM, qu'il soit lié à la cartographie, à la recherche, au développement d'applications ou à d'autres domaines spécifiques.
	* **Les exposés** : Sont généralement d'une durée de 20 minutes avec 5 minutes supplémentaires pour les questions-réponses.
	* **Exposés éclairs** : Les exposés durent généralement 5 minutes, avec 2 minutes supplémentaires pour les questions-réponses.
* **Panel** Cette session est consacrée à l'accueil d'un groupe de trois ou quatre personnes pour discuter et mettre en lumière un sujet d'intérêt particulier. Le panel dure 45 minutes, avec 15 minutes supplémentaires pour les questions-réponses.
* **Atelier** Les ateliers durent entre une heure et deux heures. Le présentateur animera des sessions pratiques qui démontrent des concepts techniques afin de permettre aux participants de suivre facilement la démonstration. Les ateliers auront lieu avant et pendant la conférence. Ils comprendront des sessions simples et introductives sur la prise en main d'OSM et des sessions plus techniques présentant des tutoriels spécifiques à la cartographie, le développement de paquets géospatiaux, la classification d'images, les applications AI/ML en cartographie.

##### Autres types de sessions
Les autres types de sessions comprennent :
* Sessions "Bird of a feather" (BoFs)
* Présentations de posters
* Sessions de cartographie par drone

<h4 id="subdetails">Détails de la Soumission</h4>
* Enregistrement et matériel
   * Cela dépend de la décision prise quant à savoir si nous aurons une conférence physique ou en ligne. Mais idéalement, l'équipe de SotM Africa prévoit d'enregistrer toutes les sessions pendant l'événement.
* Déroulement de la conférence
   * TBD
* Inscription des intervenants :
   * Pour participer à la conférence State of the Map Africa 2021, tous les intervenants et les participants devront s'inscrire à la conférence. Plus de détails seront annoncés prochainement.
   
##### Comment nous contacter
Vous pouvez nous adresser toutes vos communications à l'adresse [program@stateofthemap.africa](mailto:program@stateofthemap.africa) 

<h4 id="coc">Code de conduite</h4>
State of the Map Africa (SotM Africa) s'engage à offrir une expérience positive de la conférence à tous les participants, quels que soient leur sexe, leur identité et leur expression sexuelles, leur orientation sexuelle, leur handicap, leur apparence physique, leur race, leur âge, leur religion ou leur origine nationale ou ethnique. Nous encourageons les interactions respectueuses et attentionnées entre les participants et ne tolérons pas le harcèlement des participants à la conférence sous quelque forme que ce soit. Nous considérons que l'utilisation d'un langage offensant, la promotion du sexe et les images érotiques sont inappropriés pour toute conférence et tout réseautage. Les participants à la conférence qui enfreignent ces normes peuvent être sanctionnés ou expulsés de la conférence (sans remboursement) à la discrétion des organisateurs de la conférence. Notre politique anti-harcèlement peut être consultée [ici](/fr/code-of-conduct.md).

<h4 id="review">Règles d'examen</h4>
Vous trouverez ci-dessous les directives et les processus qui seront utilisés pour l'examen de votre proposition. Le rôle des évaluateurs est de garantir la qualité du contenu présenté à State of the Map Africa.

##### Conflit d'intérêt
En cas de conflit d'intérêt, l'évaluateur le signalera immédiatement au comité des programmes et s'engagera à se retirer de l'évaluation. Un remplacement sera également effectué rapidement

##### Critères d'évaluation
Nous évaluerons la proposition soumise sur la base des critères suivants.
* Le sujet et le contenu de la proposition sont innovants et opportuns.
* La proposition s'aligne clairement au moins sur le thème et les objectifs de la conférence.
* Les présentations proposées présentent des approches, des concepts et des stratégies qui peuvent être mis en œuvre dans toutes les disciplines.
* La proposition est concise, claire, bien organisée et traite des questions et développements actuels dans la communauté de la cartographie ouverte.
* La proposition présente des résultats d'apprentissage clairs pour les participants.


<h4 id="timelines">Calendrier</h4>
* Appel à propositions &nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**27 avril 2021**
* Date limite de soumission &nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**25 juin 2021**
* Examen des propositions &nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**30 juin - 20 juillet**
* Notification des orateurs &nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**25 juillet**
* Confirmation des intervenants &nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**1er août 2021**
* State of the Map Africa &nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**19 - 21 novembre 2021**

<h4 id="submit">Soumission</h4>
Veuillez soumettre votre proposition en utilisant [ce formulaire](https://pretalx.com/sotm-africa-2021/cfp). Toutes les heures sont en UTC.
