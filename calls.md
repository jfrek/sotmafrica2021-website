---
layout: post
title: Call for Participation 
permalink: /calls/
isStaticPost: true
image: 2017-ad.jpg
---
> [Version française](/fr/calls/)

From 19th to 21st November 2021, the OSM Africa community will be coming together for yet another exciting conference: State of the Map Africa, 2021. The conference aims to bring together OpenStreetMap enthusiasts from Africa and beyond. The conference will provide an opportunity for mappers to share and exchange ideas, knowledge, and experience; expand their network, and generate ideas to grow OSM within the continent. We are inviting members of the OpenStreetMap community and other OSM data users to share with us about your mapping projects and ideas, OSM experiences at an individual, community or institutional level. From beginner mappers, OSM data users, to community leaders, we would like to hear from you!

In addition to proposals on OSM contribution, usage, and development, with this year’s theme being Leaving no one behind: Emerging from a global pandemic, we are keen to learn about:
* Mapping projects focused on marginalized communities and minorities.
* Mapping projects that supported the fight against COVID-19. 
* How your OSM community adapted to the pandemic.
* Your individual mapping experience during the pandemic.
* Mapping collaborations across different entities, communities, and countries.

#### Navigation
* <a href="#tracks">Tracks<a/>
* <a href="#subtypes">Submission Types<a/>
* <a href="#subdetails">Submission Details<a/>
* <a href="#coc">Code of Conduct<a/>
* <a href="#review">Review Guidelines<a/>
* <a href="#timelines">Timelines<a/>
* <a href="#submit">Submission<a/>

<h4 id="tracks">Tracks</h4>
We are looking for submissions in these categories:
* **Cartography and data visualization** This track pertains to everything on map-making and data visualization. We welcome all submissions that demonstrate the use of OpenStreetMap data and platforms, tools, and software like QGIS, GRASSGIS, Blender, etc., to produce and share compelling maps and visualizations.
* **Mapping** There are several ways that one can contribute to OSM and edit the map! In this track, we would like to hear how you are using existing and new tools, mobile applications, and platforms for data collection, editing, and validation. This also includes testing experience, pros and cons of each, etc.
* **Data analysis** This track is about research, projects, and scientific work done to assess OpenStreetMap data quality, contribution patterns, and usage.
* **Community** This track is focused on presentations about open communities and networks in open mapping and the general geospatial space. From local and regional OSM communities, YouthMappers chapters, OSGeo chapters to other group networks, we would like to hear about your community activities, sustainability plans, and initiations to improve community growth, diversity, and inclusion.
* **Innovation track** Have you built web-based solutions using OSM? We would like to hear your process for designing and developing web applications, portals, and story maps using the OpenStreetMap API, different libraries, and third-party platforms.
* **Scientific computing** This track is dedicated to individuals and/or organizations who would like to share their use cases on mapping with artificial intelligence, machine learning, reality technology (AR/VR/MR), etc.

<h4 id="subtypes">Submission Types</h4>
* **Talks** This session is primarily dedicated to sharing your work with the OSM community, whether it is related to mapping, research, application development from other/specific fields.
   * Talks: Are usually 20 minutes long with additional 5 minutes for Q&A.
   * Lighting Talks: Are quick 5 minutes talks with additional 2 minutes for Q&A.
* **Panel** This session is dedicated to hosting a group of three or four to discuss and highlight a particular topic of interest. The panel is 45 minutes long, with additional 15 minutes for Q&A.
* **Workshop** Workshops will be an hour to two hours long. The presenter will facilitate hand-on sessions that demonstrate technical concepts to allow attendees to follow the demonstration easily. Workshops will take place before and during the conference. This will include simple and introductory sessions on getting started with OSM and more technical ones showing mapping-specific tutorials, geospatial package development, image classification, AI/ML applications in mapping.

##### Other session types
Other sessions types include:
* Bird of a feather sessions(BoFs)
* Poster presentations
* Drone mapping sessions

<h4 id="subdetails">Submission Details</h4>
* Recording and Material
   * This is dependent on the decision on whether we'll have a physical or online conference. But ideally, the SotM Africa team plans to record all sessions during the event.
* Conference proceeding
   * TBD
* Registration of speakers:
   * To participate at the 2021 State of the Map Africa conference, all speakers and attendees will be required to register for the conference. More details will be announced soon.

##### How to contact us
You can direct all communications to us through [program@stateofthemap.africa](mailto:program@stateofthemap.africa) 

<h4 id="coc">Code of Conduct</h4>
State of the Map Africa (SotM Africa) is dedicated to providing a positive conference experience for all attendees, regardless of gender, gender identity and expression, sexual orientation, disability, physical appearance, race, age, religion, or national and ethnic origin. We encourage respectful and considerate interactions between attendees and do not tolerate harassment of conference participants in any form. We see the use of offensive language, sexual advancement and erotic imagery as inappropriate for any conference and networking. Conference participants violating these standards may be sanctioned or expelled from the conference (without a refund) at the discretion of the conference organizers. Our anti-harassment policy can be found [here](/code-of-conduct.md).

<h4 id="review">Review Guidelines</h4>
Below are the guidelines and processes that  will be used in reviewing your submission.
The role of reviewers is to ensure the quality of the content presented at State of the Map Africa.

##### Conflict of interest
In the event of any conflict of interest, the reviewer will immediately signal to the programs committee and commit to withdraw from reviewing. A replacement will also be issued quickly.

##### Criteria for review
We shall evaluate the submitted proposal based on the following criteria.
* The proposal topic and content are innovative and timely.
* The proposal clearly aligns with at least the theme and goals of the conference.
* The proposed presentations have approaches, concepts, and strategies that can be implemented across disciplines.
* The proposal is concise, clear, well organized, and addresses current issues and development in the open mapping community.
* The proposal has clear learning outcomes for attendees.

<h4 id="timelines">Timelines</h4>
* Call for proposal&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**27th April 2021**
* Deadline for call&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**25th June  2021**
* Review of submissions&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**30th June - 20th July**
* Notification of speakers&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**25th July 2021** 
* Confirmation of speakers&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**1st August 2021**
* State of the Map Africa&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;&nbsp;**19th - 21th November, 2021**

<h4 id="submit">Submission</h4>
Please submit your proposal using [this form](https://pretalx.com/sotm-africa-2021/cfp). All times are in **UTC**.
